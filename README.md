# TD01-setup_blinky

## Setup
- Désarchiver le projet
- Installer python 3
- Installer un environnement virtuel hors du dossier du projet
```
$ python3 -m venv env
```
- Activer l'environnement virtuel
```
$ source env/bin/activate
```
- Installer mbed :
```
$ pip install mbed-cli
```

- Install toolchain : 9.2019.Q4 (decompresser l'archive dans /opt/gcc-arm-embedded/)

https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2019q4/gcc-arm-none-eabi-9-2019-q4-major-x86_64-linux.tar.bz2
- Configurer la toolchain
```
$ cd mbed-os-example-blinky
$ mbed config -G GCC_ARM_PATH "/opt/gcc-arm-embedded/gcc-arm-none-eabi-9-2019-q4-major/bin/"
```

- Il est nécessaire de : 
    - recharger l'environnement
    - aller installer les requirements dans le dossier mbed-os

- Si besoin (normalement non):
```
$ mbed target ZEST_CORE_STM32L4A6RG
$ mbed toolchain GCC_ARM
```

Compiler :
```
$ mbed compile (dans le dossier du projet)
```

## JLink

- Telecharger et installer :
https://www.segger.com/downloads/jlink/JLink_Linux_x86_64.deb

- Lancer JLinkExe :
```
$ JLinkExe -device stm32l4a6rg
> connect
```

- Flash
```
> halt
> loadfile <path/to/bin> 8000000
> reset
> go
```

- Dump
```
> savebin program.bin 8000000 <size>
```
- Erase
```
> erase [<SAddr> <EAddr>]
```
## Exercice
- Ecrire un message sur le port série (UART).
- Modifier le code pour que l'appui sur le boutton utilisateur ecrive un message sur le port série.

S'aider de la documentation de mbed os (Interruptions & Event Queue) et du fichier de configuration de la carte "pinames.h"

Doc : https://os.mbed.com/docs/mbed-os/v6.15/apis/index.html

Lecture port série :
```
$ screen /dev/ttyUSB0 9600
```
